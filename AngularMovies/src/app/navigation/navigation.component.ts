import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../shared/services/app-data.service';
import { IResults } from '../shared/services/models/movie-interface';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.sass']
})
export class NavigationComponent implements OnInit {

  movies: IResults[];
  movie: IResults;
  isSearchActive: boolean;

  constructor(private appData: AppDataService) { }

  ngOnInit() {
    this.isSearchActive = false;
  }

  search(query) {
    this.appData.getSearchResults(query.search, 1).subscribe(data => {
      this.movies = data.results;
      this.isSearchActive = true;
    });
  }
  close() {
    this.isSearchActive = false;
  }
  openDetail(movie) {
    this.movie = movie;
    console.log(this.movie);
  }

}
