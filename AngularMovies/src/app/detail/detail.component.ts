import { Component, OnInit, Input } from '@angular/core';
import { IResults } from '../shared/services/models/movie-interface';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {

  @Input() movie: IResults;

  constructor() { }

  ngOnInit() {
  }

}
