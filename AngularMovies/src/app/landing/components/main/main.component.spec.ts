import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from './main.component';
import { NowPlayingComponent } from '../now-playing/now-playing.component';
import { BestRatedComponent } from '../best-rated/best-rated.component';
import { UpcomingComponent } from '../upcoming/upcoming.component';
import { DetailComponent } from '../../../detail/detail.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../../shared.module';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent, NowPlayingComponent, BestRatedComponent, UpcomingComponent],
      imports: [HttpClientModule, SharedModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
