import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../../../shared/services/app-data.service';
import { IResults } from '../../../shared/services/models/movie-interface';

@Component({
  selector: 'app-now-playing',
  templateUrl: './now-playing.component.html',
  styleUrls: ['./now-playing.component.sass']
})
export class NowPlayingComponent implements OnInit {

  movies: IResults[];
  movie: IResults;
  totalPages;
  page;

  constructor(private appData: AppDataService) { }

  ngOnInit() {
    this.appData.getNowPlaying().subscribe(data => {
      this.movies = data.results;
    });
  }

  openDetail(movie) {
    this.movie = movie;
  }

}
