import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestRatedComponent } from './best-rated.component';
import { DetailComponent } from '../../../detail/detail.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../../shared.module';

describe('BestRatedComponent', () => {
  let component: BestRatedComponent;
  let fixture: ComponentFixture<BestRatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BestRatedComponent],
      imports: [HttpClientModule, SharedModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestRatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
