import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../../../shared/services/app-data.service';
import { IMovies, IResults } from '../../../shared/services/models/movie-interface';

@Component({
  selector: 'app-best-rated',
  templateUrl: './best-rated.component.html',
  styleUrls: ['./best-rated.component.sass']
})
export class BestRatedComponent implements OnInit {

  movies: IResults[];
  movie: IResults;

  constructor(private appData: AppDataService) { }

  ngOnInit() {
    this.appData.getBestRated(2019).subscribe(data => this.movies = data.results);
  }

  openDetail(movie) {
    this.movie = movie;
  }
}
