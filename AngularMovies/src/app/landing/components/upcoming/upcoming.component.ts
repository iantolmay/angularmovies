import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AppDataService } from '../../../shared/services/app-data.service';
import { IMovies, IResults } from 'src/app/shared/services/models/movie-interface';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.sass'],
})
export class UpcomingComponent implements OnInit {

  movies: IResults[];
  movie: IResults;
  totalPages;
  page;

  constructor(private appData: AppDataService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.getPage(1);
  }

  getPage(page) {
    this.appData.getUpcoming(page).subscribe((data: IMovies) => {
      this.movies = data.results;
      this.totalPages = data.total_pages;
    });
  }

  /**
   * FUTURE FUNCTION THAT GETS ALL THE PAGES BASED ON FILTER
   * (TMBD "DISCOVER" FEATURE NOT WORKING THEREFORE A WORKAROUND IS NEEDED)
   */
  async getNPages() {
    for (let page = 1; page < this.totalPages; page++) {
      await this.appData.getUpcoming(page).subscribe((data: IMovies) => {
        data.results.forEach(element => {
          if (this.filterMovie(element)) {
            this.movies.push(element);
          }
        });
      });
    }
  }


  filterMovie(movie: IResults): boolean {
    const now = this.transformDate(Date.now()).toString();
    const release = this.transformDate(new Date(movie.release_date));
    if (release > now) {
      return true;
    } else {
      return false;
    }
  }


  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  openDetail(movie) {
    this.movie = movie;
  }
}
