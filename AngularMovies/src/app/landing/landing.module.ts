import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { UpcomingComponent } from './components/upcoming/upcoming.component';
import { MainComponent } from './components/main/main.component';
import { BestRatedComponent } from './components/best-rated/best-rated.component';
import { DetailComponent } from '../detail/detail.component';
import { SharedModule } from '../shared.module';
import { NowPlayingComponent } from './components/now-playing/now-playing.component';


@NgModule({
  declarations: [UpcomingComponent, MainComponent, BestRatedComponent, NowPlayingComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModule
  ]
})
export class LandingModule { }
