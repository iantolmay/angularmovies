export interface IMovies {
    results: IResults[];
    page: number;
    dates: {
        maximum: number;
        minimum: number;
    };
    total_pages: number;
}

export interface IResults {
    id: number;
    poster_path: string;
    backdrop_path: string;
    genre_ids: Array<number>;
    title: string;
    overview: string;
    vote_average: number;
    release_date: Date;
}
