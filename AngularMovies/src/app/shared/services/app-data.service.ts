import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, forkJoin } from 'rxjs';
import { IMovies, IResults } from './models/movie-interface';

@Injectable({
  providedIn: 'root'
})
export class AppDataService {

  baseUrl = 'https://api.themoviedb.org/3';
  params;

  constructor(private http: HttpClient) { }

  getUpcoming(page): Observable<IMovies> {
    const params = new HttpParams().set('api_key', environment.apiKey).set('page', page);
    return this.http.get<IMovies>(`${this.baseUrl}/movie/upcoming`, { params });
  }

  getBestRated(year): Observable<IMovies> {
    let page = '1';
    const params = new HttpParams().set('api_key', environment.apiKey).set('page', page);
    let firstObservable: Observable<IMovies> = this.http.get<IMovies>(`${this.baseUrl}/movie/top_rated`, { params });
    return firstObservable;
  }

  getNowPlaying() {
    const params = new HttpParams().set('api_key', environment.apiKey);
    return this.http.get<IMovies>(`${this.baseUrl}/movie/now_playing`, { params });
  }

  getSearchResults(query, page): Observable<IMovies> {
    const params = new HttpParams().set('api_key', environment.apiKey).set('page', page).set('query', query);
    return this.http.get<IMovies>(`${this.baseUrl}/search/movie`, { params });
  }

}
